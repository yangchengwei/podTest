//
//  main.m
//  test
//
//  Created by ycw19891222@gmail.com on 10/08/2017.
//  Copyright (c) 2017 ycw19891222@gmail.com. All rights reserved.
//

@import UIKit;
#import "TRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TRAppDelegate class]));
    }
}
