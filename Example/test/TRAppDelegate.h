//
//  TRAppDelegate.h
//  test
//
//  Created by ycw19891222@gmail.com on 10/08/2017.
//  Copyright (c) 2017 ycw19891222@gmail.com. All rights reserved.
//

@import UIKit;

@interface TRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
